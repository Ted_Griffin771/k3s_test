## Origin Author: https://github.com/k3s-io/k3s-ansible

## 我的筆記

```
# Install ssh-login
sudo su  
yum install -y ansible git tree wget   
cat /dev/zero | ssh-keygen -q -N ""   
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys   
chmod 600 ~/.ssh/authorized_keys   
vi /etc/ssh/sshd_config     //修改ssh允許可使用root帳號登入   
PermitRootLogin yes         
PubkeyAuthentication yes  

#restart service  
systemctl restart sshd
```
```
# Install k3s
git clone https://github.com/k3s-io/k3s-ansible.git  
cd k3s-ansible/  
cp -R inventory/sample inventory/my-cluster  
vim inventory/my-cluster/hosts.ini  

# edit ansible_user  
vim inventory/my-cluster/group_vars/all.yml 
ansible-playbook site.yml -i inventory/my-cluster/hosts.ini  
free | grep Mem: | tr -s ' ' | cut -d' ' -f3  #master need 630 mb memory  
```

```
# GCP自動生產密鑰方式
sudo su
cat /dev/zero | ssh-keygen -q -N ""   

gcloud compute os-login ssh-keys add \
    --key-file= <path_of_id_rsa.pub> \
    --project= <project_id> \
    --ttl=365d
    
手動複製將公鑰放到中繼資料的安全殼層金鑰中  
#注意目標VM的id_rsa.pub與authorized_keys,ssh-copy-id
```


## 2021/11/29測試混合OS佈署結果
host          | operating system | specification | status |
--------------|:-----:|:-----:|:-----:|
master|centos7|e2-micro-1.0GB|standard|
worker1|centos7|f1-micro-0.6GB|spot
worker2|centos8|f1-micro-0.6GB|spot
worker3|ubuntu20.04|f1-micro-0.6GB|standard|

<img src="./tmp/success.png" width="60%" height="60%">

